const RtcTokenBuilder = require('../src/RtcTokenBuilder').RtcTokenBuilder;
const RtcRole = require('../src/RtcTokenBuilder').Role;

const appID = '9619030e71064032a5ae66eb44daa5d6';
const appCertificate = '6d94d418c28d4e0cb2850f896a07b6ca';
const channelName = 'doctor';
//const uid = 2882341273;
const account = "developer.telemedicina@gmail.com0";
const role = RtcRole.PUBLISHER;

//const expirationTimeInSeconds = 3600
const expirationTimeInSeconds = 120

const currentTimestamp = Math.floor(Date.now() / 1000)

const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds

// IMPORTANT! Build token with either the uid or with the user account. Comment out the option you do not want to use below.

// Build token with uid
/*const tokenA = RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, channelName, uid, role, privilegeExpiredTs);
console.log("Token With Integer Number Uid: " + tokenA);*/

// Build token with user account
const tokenB = RtcTokenBuilder.buildTokenWithAccount(appID, appCertificate, channelName, account, role, privilegeExpiredTs);
console.log("Token With UserAccount: " + tokenB);
